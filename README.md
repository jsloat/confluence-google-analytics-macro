These files are meant to be run on Heroku as a Python Flask server.

The URL for the AC JSON file is {hostname}/atlassian-connect.json, as defined in the Flask app file con-ga-macro.py

This code is currently (May 28, 2014) being served on Heroku at the URL http://confluence-ga-macro.herokuapp.com. The following pages are currently accessible:

* http://confluence-ga-macro.herokuapp.com/request?freq=day
* http://confluence-ga-macro.herokuapp.com/request?freq=month
* http://confluence-ga-macro.herokuapp.com/atlassian-connect.json

See the ShipIt issue for more context, screenshots, and next steps: https://shipit.atlassian.net/browse/SHPXXVII-64