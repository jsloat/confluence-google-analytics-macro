import os
from flask import Flask,request

app = Flask(__name__)

@app.route('/')
def hello():
    return 'Hello World!'

@app.route('/request')
def output():
	frequency = request.args.get('freq')

	if frequency == 'month':
		return """
		<table>
<tbody>
  <tr>
			<th>	Month	</th><th>	Sessions	</th>
			</tr><tr>
<td>	May</td><td>3,771,871</td>
 </tr>
</tbody>
</table>"""

	if frequency == 'day':
		return """
		<table>
<tbody>
  <tr>
			<th>	Day	</th><th>	Sessions	</th>
			</tr><tr>
<td>	4/27/14	</td><td>	53,323	</td>
</tr><tr>
<td>	4/28/14	</td><td>	149,121	</td>
</tr><tr>
<td>	4/29/14	</td><td>	158,222	</td>
</tr><tr>
<td>	4/30/14	</td><td>	146,465	</td>
</tr><tr><td>	5/1/14	</td><td>	111,952	</td>
</tr><tr><td>	5/2/14	</td><td>	106,326	</td>
</tr><tr><td>	5/3/14	</td><td>	39,502	</td>
</tr><tr><td>	5/4/14	</td><td>	55,718	</td>
</tr><tr><td>	5/5/14	</td><td>	156,316	</td>
</tr><tr><td>	5/6/14	</td><td>	172,381	</td>
</tr><tr><td>	5/7/14	</td><td>	163,995	</td>
</tr><tr><td>	5/8/14	</td><td>	154,809	</td>
</tr><tr><td>	5/9/14	</td><td>	125,199	</td>
</tr><tr><td>	5/10/14	</td><td>	40,380	</td>
</tr><tr><td>	5/11/14	</td><td>	58,572	</td>
</tr><tr><td>	5/12/14	</td><td>	164,968	</td>
</tr><tr><td>	5/13/14	</td><td>	168,585	</td>
</tr><tr><td>	5/14/14	</td><td>	167,081	</td>
</tr><tr><td>	5/15/14	</td><td>	160,522	</td>
</tr><tr><td>	5/16/14	</td><td>	122,057	</td>
</tr><tr><td>	5/17/14	</td><td>	37,199	</td>
</tr><tr><td>	5/18/14	</td><td>	57,445	</td>
</tr><tr><td>	5/19/14	</td><td>	163,365	</td>
</tr><tr><td>	5/20/14	</td><td>	172,627	</td>
</tr><tr><td>	5/21/14	</td><td>	174,432	</td>
</tr><tr><td>	5/22/14	</td><td>	175,592	</td>
</tr><tr><td>	5/23/14	</td><td>	128,879	</td>
</tr><tr><td>	5/24/14	</td><td>	39,545	</td>
</tr><tr><td>	5/25/14	</td><td>	57,229	</td>
</tr><tr><td>	5/26/14	</td><td>	118,085	</td>
</tr><tr><td>	5/27/14	</td><td>	171,979	</td>
 </tr>
</tbody>
</table>
"""

@app.route('/atlassian-connect.json')
def AC_app():
	return """
{
  "name": "Google Analytics Macro",
  "description": "Pull GA data into a Confluence table.",
  "key": "com.jsloat.conf-ga-macro",
  "baseUrl": "http://localhost:5000",
  "vendor": {
    "name": "John Sloat",
    "url": "http://sloaticus.com"
  },
  "authentication": {
    "type": "none"  
  },
  "version": "1.0",
  "modules": {
    "staticContentMacros": [
      {
        "key": "Google-analytics-macro",
        "name": {
          "value": "Google Analytics Macro"
        },
        "featured": true,
        "categories": [
          "external-content"
        ],
        "url": "/request?freq={freq}",
        "description": {
          "value": "This macro makes it fuckin so simple to get GA data in here."
        },
        "parameters": [
          {
           "identifier": "freq",
      "name": {
        "value": "Data Frequency"
      },
      "description": {
        "value": "Choose frequency of data returned."
      },
      "type": "enum",
      "values": [
        "day",
        "month"
      ]
        }
        ],
        "outputType": "block",
        "bodyType": "none"
      }
    ]
  }
}
"""
